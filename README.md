DEXP Ixion P350
==============

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | 1.3GHz Quad-Core MT6580
GPU     | Mali-400MP
Memory  | 1GB RAM
Shipped Android Version | 5.1
Storage | 8GB
Battery | 5000 mAh
Display | 5" 1280 x 720 px
Camera  | 8MPx, LED Flash

![DEXP](https://content2.onliner.by/catalog/device/main/28756eabf91d930d247cb06211958125.jpeg "DEXP Ixion P350 Tundra Black")

This branch is for building AICP 10.0 (or Android Lollipop 5.1 CM based roms) ROMS.

Working:
- Audio
- Sensors
- Wi-Fi
- Camera
- Modem
- USSD
- Bluetooth
- Mobile data
- Wi-Fi tethering
- GPS
- HW composer
- Sleep

Not working/Bugs:
- Video not played

Thanks:
@Besik13
@dee3000
@Zormax
